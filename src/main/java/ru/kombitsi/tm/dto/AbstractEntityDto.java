package ru.kombitsi.tm.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public abstract class AbstractEntityDto {
    private String id = UUID.randomUUID().toString();
}
