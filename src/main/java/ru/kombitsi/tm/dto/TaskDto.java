package ru.kombitsi.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.kombitsi.tm.entity.AbstractEntity;
import ru.kombitsi.tm.enumerate.Status;

import java.util.Date;

@Getter
@Setter
public final class TaskDto extends AbstractEntity {

    private String name;

    private String description;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateAdd = new Date();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    private Status displayName = Status.SCHEDULE;

    private String projectId;
}

