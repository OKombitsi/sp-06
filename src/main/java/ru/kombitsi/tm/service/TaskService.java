package ru.kombitsi.tm.service;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kombitsi.tm.api.ITaskService;
import ru.kombitsi.tm.dto.TaskDto;
import ru.kombitsi.tm.repository.TaskRepository;
import ru.kombitsi.tm.util.DtoConvert;

import java.util.List;

@Getter
@Setter
@Service
@Transactional
@NoArgsConstructor
public class TaskService implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    @Nullable
    public List<TaskDto> showAllTaskByAllUsers() {
        return DtoConvert.taskListToDtoList(taskRepository.findAll());
    }

    @Override
    @Nullable
    public TaskDto getTaskById(@NotNull String id) throws Exception {
        if (id == null) return null;
        return DtoConvert.taskToDto(taskRepository.getOne(id));
    }

    @Override
    @Nullable
    public TaskDto updateTask(@NotNull TaskDto taskDto) {
        if (taskDto == null) return null;
        return DtoConvert.taskToDto(taskRepository.save(DtoConvert.dtoToTask(taskDto)));
    }

    @Override
    public void removeOneTask(@NotNull TaskDto taskDto) {
        if (taskDto == null) return;
        taskRepository.delete(DtoConvert.dtoToTask(taskDto));
    }
}

