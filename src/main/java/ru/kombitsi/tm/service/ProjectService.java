package ru.kombitsi.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kombitsi.tm.api.IProjectService;
import ru.kombitsi.tm.dto.ProjectDto;
import ru.kombitsi.tm.repository.ProjectRepository;
import ru.kombitsi.tm.util.DtoConvert;

import java.util.List;

@Getter
@Setter
@Service
@Transactional
public class ProjectService implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @Nullable
    public List<ProjectDto> listAllProject() {
        return DtoConvert.projectListToDtoList(projectRepository.findAll());
    }

    @Override
    public ProjectDto updateProject(@NotNull ProjectDto project) {
        if (project == null) return null;
        return DtoConvert.projectToDTO(projectRepository.saveAndFlush(DtoConvert.dtoToProject(project)));
    }

    @Override
    @Nullable
    public ProjectDto findOneProjectById(@NotNull String id) {
        if (id.isEmpty() || id == null) return null;
        return DtoConvert.projectToDTO(projectRepository.getOne(id));
    }

    @Override
    public void removeOneProject(@NotNull ProjectDto projectDto) {
        if (projectDto == null) return;
        projectRepository.delete(DtoConvert.dtoToProject(projectDto));
    }
}

