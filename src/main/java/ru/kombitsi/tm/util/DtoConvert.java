package ru.kombitsi.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombitsi.tm.dto.ProjectDto;
import ru.kombitsi.tm.dto.TaskDto;
import ru.kombitsi.tm.entity.Project;
import ru.kombitsi.tm.entity.Task;
import java.util.ArrayList;
import java.util.List;

public final class DtoConvert {

    @Nullable
    public static Project dtoToProject(@NotNull final ProjectDto projectDTO) {
        Project project = new Project();
        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setDateAdd(projectDTO.getDateAdd());
        project.setDateStart(projectDTO.getDateStart());
        project.setDateFinish(projectDTO.getDateFinish());
        project.setDisplayName(projectDTO.getDisplayName());
        return project;
    }
    @Nullable
    public static ProjectDto projectToDTO(@NotNull final Project project) {
        ProjectDto projectDTO = new ProjectDto();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateAdd(project.getDateAdd());
        projectDTO.setDateStart(project.getDateStart());
        projectDTO.setDateFinish(project.getDateFinish());
        projectDTO.setDisplayName(project.getDisplayName());
        return projectDTO;
    }

    @Nullable
    public static Task dtoToTask(@NotNull final TaskDto taskDto) {
        Task task = new Task();
        task.setId(taskDto.getId());
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setDateAdd(taskDto.getDateAdd());
        task.setDateStart(taskDto.getDateStart());
        task.setDateFinish(taskDto.getDateFinish());
        task.setDisplayName(taskDto.getDisplayName());
        Project project = new Project();
        project.setId(taskDto.getProjectId());
        task.setProject(project);
        return task;
    }

    @Nullable
    public static TaskDto taskToDto(@NotNull final Task task) {
        TaskDto taskDTO = new TaskDto();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateAdd(task.getDateAdd());
        taskDTO.setDateStart(task.getDateStart());
        taskDTO.setDateFinish(task.getDateFinish());
        taskDTO.setDisplayName(task.getDisplayName());
        taskDTO.setProjectId(task.getProject().getId());
        return taskDTO;
    }

    @Nullable
    public static List<Project> dtoListToProjectList(@NotNull final List<ProjectDto> projectDtoList) {
        List<Project> projectList = new ArrayList<>();
        for (ProjectDto projectDto : projectDtoList) {
            Project project = dtoToProject(projectDto);
            projectList.add(project);
        }
        return projectList;
    }

    @Nullable
    public static List<Task> dtoListToTaskList(@NotNull final List<TaskDto> taskDtoList) {
        List<Task> taskList = new ArrayList<>();
        for (TaskDto taskDto : taskDtoList) {
            Task task = dtoToTask(taskDto);
            taskList.add(task);
        }
        return taskList;
    }

    @Nullable
    public static List<ProjectDto> projectListToDtoList(@NotNull final List<Project> projectList) {
        List<ProjectDto> projectDtoList = new ArrayList<>();
        for (Project project : projectList) {
            ProjectDto projectDto = projectToDTO(project);
            projectDtoList.add(projectDto);
        }
        return projectDtoList;
    }

    @Nullable
    public static List<TaskDto> taskListToDtoList(@NotNull final List<Task> taskList) {
        List<TaskDto> taskDtoList = new ArrayList<>();
        for (Task task : taskList) {
            TaskDto taskDto = taskToDto(task);
            taskDtoList.add(taskDto);
        }
        return taskDtoList;
    }
}
