package ru.kombitsi.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombitsi.tm.dto.TaskDto;

import java.util.List;

public interface ITaskService {
    @Nullable
    List<TaskDto> showAllTaskByAllUsers();

    @Nullable
    TaskDto getTaskById(@NotNull String id) throws Exception;

    @Nullable
    TaskDto updateTask(@NotNull TaskDto taskDto);

    void removeOneTask(@NotNull TaskDto taskDto);
}
