package ru.kombitsi.tm.controller;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kombitsi.tm.api.ITaskService;
import ru.kombitsi.tm.dto.TaskDto;

import java.util.List;


@Getter
@Setter
@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @GetMapping("/showall")
    public List<TaskDto> getAllTasks() {
        List<TaskDto> taskDtoList = taskService.showAllTaskByAllUsers();
        return taskDtoList;
    }

    @GetMapping("/{id}")
    public TaskDto getOneTaskById(@PathVariable("id") String id) throws Exception {
        return taskService.getTaskById(id);
    }

    @PostMapping
    public TaskDto createTask(@RequestBody TaskDto taskDto) {
        return taskService.updateTask(taskDto);
    }

    @PutMapping("/update/{id}")
    public TaskDto updateTask(@PathVariable (value = "id") String id, @RequestBody TaskDto taskDetail) throws Exception {
        TaskDto taskDto = taskService.getTaskById(id);
        taskDto.setName(taskDetail.getName());
        taskDto.setDescription(taskDetail.getDescription());
        return taskService.updateTask(taskDto);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteTask(@PathVariable(value = "id") String id) throws Exception {
        TaskDto taskDto = taskService.getTaskById(id);
        taskService.removeOneTask(taskDto);
    }
}

