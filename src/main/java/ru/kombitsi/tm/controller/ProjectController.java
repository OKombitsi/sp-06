package ru.kombitsi.tm.controller;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kombitsi.tm.api.IProjectService;
import ru.kombitsi.tm.dto.ProjectDto;

import java.util.List;


@Getter
@Setter
@RestController
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("showall")
    public List<ProjectDto> getAllProjects() {
        List<ProjectDto> projectDtoList = projectService.listAllProject();
        return projectDtoList;
    }

    @GetMapping("/{id}")
    public ProjectDto getOneProjectById(@PathVariable("id") String id) {
        return projectService.findOneProjectById(id);
    }

    @PostMapping
    public ProjectDto createProject(@RequestBody ProjectDto projectDto) {
        return projectService.updateProject(projectDto);
    }

    @PutMapping("/update/{id}")
    public ProjectDto updateProject(@PathVariable (value = "id") String id, @RequestBody ProjectDto projectDetail) {
        ProjectDto projectDto = projectService.findOneProjectById(id);
        projectDto.setName(projectDetail.getName());
        projectDto.setDescription(projectDetail.getDescription());
        return projectService.updateProject(projectDto);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteProject(@PathVariable(value = "id") String id) {
        ProjectDto projectDto = projectService.findOneProjectById(id);
        projectService.removeOneProject(projectDto);
    }
}

