package ru.kombitsi.tm.repository;

import lombok.Getter;
import lombok.Setter;
import ru.kombitsi.tm.entity.AbstractEntity;

@Setter
@Getter
public abstract class AbstractRepository<T extends AbstractEntity> {
}
