package ru.kombitsi.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kombitsi.tm.entity.Task;

public interface TaskRepository extends JpaRepository<Task, String> {
}
