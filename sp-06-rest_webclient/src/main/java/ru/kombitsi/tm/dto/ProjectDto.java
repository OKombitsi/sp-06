package ru.kombitsi.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.kombitsi.tm.entity.AbstractEntity;
import ru.kombitsi.tm.enumerate.Status;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Getter
@Setter
@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.FIELD)
public final class ProjectDto extends AbstractEntity {

    private String name;

    private String description;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateAdd = new Date();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    private Status displayName = Status.SCHEDULE;
}
