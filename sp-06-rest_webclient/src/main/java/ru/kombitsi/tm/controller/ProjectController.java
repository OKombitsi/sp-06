package ru.kombitsi.tm.controller;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import ru.kombitsi.tm.dto.ProjectDto;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ProjectController {
    final String URL = "http://localhost:8080/projects";

    @GetMapping("/projects/showall")
    public List<ProjectDto> getAllProjects() {
        final RestTemplate restTemplate = new RestTemplate();
        final ProjectDto[] projects = restTemplate.getForObject(URL+"/showall", ProjectDto[].class);
        return Arrays.asList(projects);
    }

    @GetMapping("/projects/{id}")
    public ProjectDto getOneProjectById(@PathVariable(name = "id") String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final ProjectDto projectDto = restTemplate.getForObject(URL+"/"+id, ProjectDto.class);
        return projectDto;
    }

    @GetMapping("/projects")
    public void createProject(@RequestParam String name, @RequestParam String description) {
        final RestTemplate restTemplate = new RestTemplate();
        ProjectDto projectDto = new ProjectDto();
        projectDto.setName(name);
        projectDto.setDescription(description);
        restTemplate.postForObject(URL, projectDto, ProjectDto.class);
    }

    @GetMapping("/projects/update")
    public void updateProject(@RequestParam String id, @RequestParam String name, @RequestParam String description ) {
        Map<String, String> params = new LinkedHashMap<String, String>();
        params.put("id", id);
        final RestTemplate restTemplate = new RestTemplate();
        ProjectDto projectDto = new ProjectDto();
        projectDto.setName(name);
        projectDto.setDescription(description);
        restTemplate.put(URL+"/update/{id}", projectDto, params);
    }

    @GetMapping("projects/delete")
    public void deleteProject(@RequestParam String id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL+"/delete/{id}", id);
    }
}
